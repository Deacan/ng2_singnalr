﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalRHub
{
  public static class Constants
  {
    public const string AdminChannel = "admin";
    public const string TaskChannel = "tasks";
    public const string JobChannel = "jobs";
  }
}

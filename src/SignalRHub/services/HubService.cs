﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SignalRHub.models;
using Microsoft.AspNet.SignalR;
using System.Threading;

namespace SignalRHub.services
{
  public class HubService
  {
    public HubService()
    {

    }

    public void ExecuteTask(IHubContext context, string channel, string eventName, double steps)
    {
      var status = new Status
      {
        State = "starting",
        PercentComplete = 0.0
      };

      PublishEvent(context, channel, eventName, status);

      for (double i = 0; i < steps; i++)
      {
        // Update the status and publish a new event
        //
        status.State = "working";
        status.PercentComplete = (i / steps) * 100;
        PublishEvent(context, channel, eventName, status);

        Thread.Sleep(500);
      }

      status.State = "complete";
      status.PercentComplete = 100;
      PublishEvent(context, channel, eventName, status);
    }

    public void PublishEvent(IHubContext context, string channel, string eventName, Status status)
    {
      // From .NET code like this we can't invoke the methods that
      //  exist on our actual Hub class...because we only have a proxy
      //  to it. So to publish the event we need to call the method that
      //  the clients will be listening on.
      //
      context.Clients.Group(channel).OnEvent(channel, new ChannelEvent
      {
        ChannelName = channel,
        Name = eventName,
        Data = status
      });
    }
  }
}

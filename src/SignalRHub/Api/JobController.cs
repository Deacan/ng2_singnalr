﻿using Microsoft.AspNet.SignalR;
using Serilog;
using SignalRHub.services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace SignalRHub.Api
{
  [RoutePrefix("jobs")]
  public class JobController: ApiController
  {
    private IHubContext _context;
    private HubService _hubservice;
    private string _channel = Constants.JobChannel;

    public JobController()
    {
      _context = GlobalHost.ConnectionManager.GetHubContext<EventHub>();
      _hubservice = new HubService();
    }

    [Route("medium")]
    [HttpGet]
    public IHttpActionResult GetLongTask()
    {
      Log.Information("Starting Job task");

      double steps = 10;
      var eventName = "job.status";

      _hubservice.ExecuteTask(_context, _channel, eventName, steps);

      return Ok("Job complete");
    }
  }
}

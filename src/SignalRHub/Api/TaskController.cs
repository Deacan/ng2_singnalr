﻿using Microsoft.AspNet.SignalR;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using SignalRHub.models;
using System.Threading;
using SignalRHub.services;

namespace SignalRHub.Api
{
  [RoutePrefix("tasks")]
  public class TaskController : ApiController
  {
    private IHubContext _context;
    private HubService _hubservice;

    // This can be defined a number of ways
    //
    private string _channel = Constants.TaskChannel;

    public TaskController()
    {
      // Normally we would inject this
      //
      _context = GlobalHost.ConnectionManager.GetHubContext<EventHub>();
      _hubservice = new HubService();
    }


    [Route("long")]
    [HttpGet]
    public IHttpActionResult GetLongTask()
    {
      Log.Information("Starting long task");

      double steps = 10;
      var eventName = "longTask.status";

      _hubservice.ExecuteTask(_context, _channel, eventName, steps);

      return Ok("Long task complete");
    }



    [Route("short")]
    [HttpGet]
    public IHttpActionResult GetShortTask()
    {
      Log.Information("Starting short task");

      double steps = 5;
      var eventName = "shortTask.status";

      _hubservice.ExecuteTask(_context, _channel, eventName, steps);

      return Ok("Short task complete");
    }
  }
}

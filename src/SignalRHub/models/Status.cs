﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalRHub.models
{
  public class Status
  {
    public string State { get; set; }
    public double PercentComplete { get; set; }
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutRoutingModule } from './about/about-routing.module';
import { HomeRoutingModule } from './home/home-routing.module';

const routes: Routes = [];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [RouterModule,
    AboutRoutingModule,
    HomeRoutingModule]
})
export class AppRoutingModule { }


import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ConnectableObservable } from 'rxjs/observable/ConnectableObservable';
import { Subject } from 'rxjs/Subject';
import {
  ConnectionState,
  ChannelSubject,
  SignalrWindow,
  ChannelConfig,
  ChannelEvent
} from '../models/index';

/**
 * ChannelService is a wrapper around the functionality that SignalR
 * provides to exspose the idea of channels and events. With this serivce
 * you can subscribe to specific channels (or groups in signalr speak) and
 * use observables to react to specific events sent out on these channels.
 */
@Injectable()
export class ChannelService {

  /**
   * starting$ is an observerable available to know if the signalr
   * connection is ready or not. On a successful connection this stream
   * will emit a value.
   */
  starting$: Observable<any>;

  /**
   * connectionState$ provides the current state of the underlying
   * connection as an Observable stream.
   */
  connectionState$: Observable<ConnectionState>;

  /**
   * error$ provides a stram of any error messages that occur on the
   * SignalR connection.
   */
  error$: Observable<string>;

  public startingSubject = new Subject<any>();

  // These are use to feed the public Observables.
  private connectionStateSubject = new Subject<ConnectionState>();
  private errorSubject = new Subject<any>();

  // These are used to track internal SignalR state.
  private hubConnection: any;
  private hubProxy: any;

  // An internal array to track what channel subscriptions exist.
  private _subjects = new Array<ChannelSubject>();

  public set subjects(value: Array<ChannelSubject>) {
    this._subjects = value;
  }

  public get subjects() {
    return this._subjects;
  }


  constructor( @Inject(SignalrWindow) private window: SignalrWindow,
    @Inject('channel.config') private channelConfig: ChannelConfig) {
    if (this.window.$ === undefined || this.window.$.hubConnection === undefined) {
      throw new Error(`The variable \'$\' or the .hubConnection() function are not defined...
      please check the SignalR scripts have been loaded properly`);
    }

    // Sey up our Observables.
    this.connectionState$ = this.connectionStateSubject.asObservable();
    this.error$ = this.errorSubject.asObservable();
    this.starting$ = this.startingSubject.asObservable();

    this.hubConnection = this.window.$.hubConnection();
    this.hubConnection.url = this.channelConfig.url;
    this.hubProxy = this.hubConnection.createHubProxy(this.channelConfig.hubName);

    // Define handlers for the connection state.
    this.hubConnection.stateChanged((state: any) => {
      let newState = ConnectionState.Connecting;

      switch (state.newState) {
        case this.window.$.signalR.connectionState.connecting:
          newState = ConnectionState.Connecting;
          break;
        case this.window.$.signalR.connectionState.connected:
          newState = ConnectionState.Connected;
          break;
        case this.window.$.signalR.connectionState.reconnection:
          newState = ConnectionState.Reconnecting;
          break;
        case this.window.$.signalR.connectionState.disconnected:
          newState = ConnectionState.Disconnected;
          break;
        default:
          break;
      }
      // Push the new state on our subject.
      this.connectionStateSubject.next(newState);
    });

    // Define handlers for ant errors.
    this.hubConnection.error((error: any) => {
      // Push the error on our subject.
      this.errorSubject.next(error);
    });

    this.hubProxy.on('OnEvent', (chanel: string, ev: ChannelEvent) => {
      /**
       * This method acts like a broker for incoming messages. We
       * check the internal array of subjects to see if one exists
       * for the channel that this came in on, and then emit the event
       * on it. Otherwise we ignore the message.
       */
      let channelSub = this.subjects.find((x: ChannelSubject) => {
        return x.channel === chanel;
      }) as ChannelSubject;

      if(channelSub !== undefined) {
        return channelSub.subject.next(ev);
      }
    });
  }

  /**
   * Start the SignalR connection. The starting$ stream will emit an
   * event if the connection is established, otherwise it will emit an
   * error.
   */
  start(): void {
    /**
     * Now we only want the connection started once, so we have a special
     * starting$ observable that the clients can subscribe to, to know
     * if the start up sequence is done.
     *
     * If we just mapped the start() promise to an observable, then any time
     * a client subscribed to it, the start sequence would be triggered
     * again since it's a cold observable.
     */
    this.hubConnection.start()
      .done(() => {
        this.startingSubject.next();
      })
      .fail((error: any) => {
        this.startingSubject.error(error);
      });
  }

  /**
   * Get an observable that will contain the data associated with a specific
   * channel.
   */
  sub(channel: string): Observable<ChannelEvent> {
    /**
     * Try and find an observable that we already created for the requested
     * channel
     */
    let channelSub = this.subjects.find((x: ChannelSubject) => {
      return x.channel === channel;
    }) as ChannelSubject;

    // If we already have one for this event, then just return it.
    if (channelSub !== undefined) {
      console.log(`Found exsisting observable for ${channel} channel`);
      return channelSub.subject.asObservable();
    }

    /**
     * If we're here then we don't already have the observable too provide the
     * caller, so we need to call the server method to join the channel
     * and then create an observable that caller can use to receive messages.
     *
     * Now we just create our internal object so we can track this subject
     * in case someone else wants it too.
     */
    channelSub = new ChannelSubject();
    channelSub.channel = channel;
    channelSub.subject = new Subject<ChannelEvent>();
    this.subjects.push(channelSub);

    /**
     * Now SignalR is asynchrounous, so we need to ensure the connection is
     * established before we call any server methods. So we'll subscribe to
     * the starting$ stream since that wont emit a value the connection
     * is ready.
     */
    this.starting$.subscribe(() => {
      this.hubProxy.invoke('Subscribe', channel)
        .done(() => {
          console.log(`Successfully subscribed to ${channel} channel`);
        })
        .fail((error: any) => {
          channelSub.subject.error(error);
        });
    },
      (error: any) => {
        channelSub.subject.error(error);
      });

    return channelSub.subject.asObservable();
  }

  /**
   * publish provides a way for calles to emit events on any channel. In a
   * production app the server would ensure that only authorized clients can
   * actually emit the message, but here we're not concerned about that.
   */
  publish(ev: ChannelEvent): void {
    this.hubProxy.invoke('Publish', ev);
  }

}

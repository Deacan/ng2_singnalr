import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()
export class ApiService {

  constructor(private http: Http) { }

  CallApi(apiUrl: string): Observable<any> {
    return this.http.get(apiUrl)
      .map((response: Response) => {
        return response.json();
      });
  }
}

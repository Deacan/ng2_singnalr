import { NgModule } from '@angular/core';
import {
  ChannelService,
  ApiService
} from './index';


@NgModule({
  providers: [ChannelService,
    ApiService]
})
export class ServicesModule { }

import { ChannelConfig } from '../models/index';

export let channelConfig = new ChannelConfig();
channelConfig.url = 'http://localhost:9123/signalr';
channelConfig.hubName = 'EventHub';

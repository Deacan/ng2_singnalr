import { ChannelEvent } from './index';
import { ChannelSubjectInterface } from './interfaces/index';
import { Subject } from 'rxjs/Subject';

export class ChannelSubject implements ChannelSubjectInterface {
  channel: string;
  subject: Subject<ChannelEvent>;

  constructor(obj?: ChannelSubject) {
    this.channel = obj && obj.channel || null;
    this.subject = obj && obj.subject || new Subject<ChannelEvent>();
  }
}

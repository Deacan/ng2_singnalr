import { SignalrWindowInterface } from './interfaces/index';


export class SignalrWindow extends Window implements SignalrWindowInterface {
  $: any;

  constructor(obj?: SignalrWindow) {
    super();
    this.$ = obj && obj.$ || null;
  }
}

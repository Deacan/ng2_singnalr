import { ChannelConfigInterface } from './interfaces/index';


export class ChannelConfig implements ChannelConfigInterface {
  url: string;
  hubName: string;
  channel: string;

  constructor(obj?: ChannelConfig) {
    this.url = obj && obj.url || null;
    this.hubName = obj && obj.hubName || null;
    this.channel = obj && obj.channel || null;
  }
}

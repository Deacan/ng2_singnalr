export interface StatusEventInterface {
  State: string;
  PercentageComplete: number;
}

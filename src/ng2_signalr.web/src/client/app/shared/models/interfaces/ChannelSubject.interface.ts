import { ChannelEventInterface } from './index';
import { Subject } from 'rxjs/Subject';

export interface ChannelSubjectInterface {
  channel: string;
  subject: Subject<ChannelEventInterface>;
}


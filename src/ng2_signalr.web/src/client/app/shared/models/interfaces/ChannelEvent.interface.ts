export interface ChannelEventInterface {
  Name: string;
  ChannelName: string;
  Timestamp: Date;
  Data: any;
  Json: string;
}

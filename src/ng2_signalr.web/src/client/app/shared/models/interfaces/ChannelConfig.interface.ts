export interface ChannelConfigInterface {
  url: string;
  hubName: string;
  channel: string;
}

import { ChannelEventInterface } from './interfaces/index';


export class ChannelEvent implements ChannelEventInterface {
  Name: string;
  ChannelName: string;
  Timestamp: Date;
  Data: any;
  Json: string;

  constructor(obj?: ChannelEvent) {
    this.Name = obj && obj.Name || null;
    this.ChannelName = obj && obj.ChannelName || null;
    this.Timestamp = obj && obj.Timestamp || new Date();
    this.Data = obj && obj.Data || null;
    this.Json = obj && obj.Json || null;
  }
}

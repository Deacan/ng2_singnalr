import { StatusEventInterface } from './interfaces/index';

export class StatusEvent implements StatusEventInterface {
  State: string;
  PercentageComplete: number;

  constructor(obj?: StatusEvent) {
    this.State = obj && obj.State || null;
    this.PercentageComplete = obj && obj.PercentageComplete || null;
  }
}

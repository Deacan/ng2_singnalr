import { ChannelService } from './../shared/services/index';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { ConnectionState } from '../shared/models/index';
import { Config } from '../shared/config/env.config';


/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {

  /**
   * An internal "copy" of the connection state stream used to because
   * we want to map the values of the original stream. If we didn't
   * we need to do that then we could the service's observerable
   * right in the template.
   */
  connectionState$: Observable<string>;
  longTask: string = Config.API + Config.LongTaskUrl;
  shortTask: string = Config.API + Config.ShortTaskUrl;
  mediumJob: string = Config.API + Config.MediumJobUrl;

  constructor(private channelService: ChannelService) {
    // Lets wire up the SingnalR observerables.
    this.connectionState$ = this.channelService.connectionState$
      .map((state: ConnectionState) => {
        return ConnectionState[state];
      });

    this.channelService.error$.subscribe(
      (error: any) => { console.warn(error); },
      (error: any) => { console.error('errors$ error', error); }
    );

    /**
     * Wire up a handler for the starting$ observerable to the
     * success/fail result.
     */
    this.channelService.starting$.subscribe(
      () => { console.log('SignalR service has been started'); },
      () => { console.warn('SignalR service failed to start'); }
    );
  }

  ngOnInit() {
    // Start the connection up.
    console.log('Starting the channel service');
    this.channelService.start();
  }

}

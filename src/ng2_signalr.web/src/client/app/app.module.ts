
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF } from '@angular/common';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AboutModule } from './about/about.module';
import { HomeModule } from './home/home.module';
import { TaskModule } from './task/task.module';
import { SharedModule } from './shared/shared.module';
import { SignalrWindow } from './shared/models/index';
import { channelConfig } from './shared/config/signalr.config';

@NgModule({
  imports: [BrowserModule,
    HttpModule,
    AppRoutingModule,
    AboutModule,
    HomeModule,
    SharedModule.forRoot(),
    TaskModule
  ],
  declarations: [AppComponent],
  providers: [{
    provide: APP_BASE_HREF,
    useValue: '<%= APP_BASE %>'
  },
  { provide: SignalrWindow, useValue: window },
  { provide: 'channel.config', useValue: channelConfig }
  ],
  bootstrap: [AppComponent]

})
export class AppModule { }

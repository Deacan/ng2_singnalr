import { Observable } from 'rxjs/Observable';

import { ChannelEvent } from './../shared/models/index';
import {
  ChannelService,
  ApiService
} from './../shared/services/index';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'sd-task',
  templateUrl: './task.component.html'
})
export class TaskComponent implements OnInit {
  @Input() eventName: string;
  @Input() apiUrl: string;
  @Input() newChannel: boolean;

  messages = '';
  private channel = 'tasks';

  constructor(private channelService: ChannelService, private apiService: ApiService) { }

  ngOnInit() {
    console.log('TaskComponent');
    this.createSubscription(this.channel);
  }

  // Subscribe to Observable
  createSubscription(channel: string) {
    // Get an observable for the events emitted on this channel.
    return this.channelService.sub(channel).subscribe(
      (x: ChannelEvent) => {
        switch (x.Name) {
          case this.eventName: { this.appendStatusUpdate(x); }
        }
      },
      (error: any) => {
        console.warn('Attempt to join channel failed!', error);
      }
    );
  }

  callApi() {
    if (!this.newChannel) {
      this.apiService.CallApi(this.apiUrl).subscribe((message: string) => {
        console.log(message);
      });
    } else {
      this.createSubscription('jobs');
      this.channelService.startingSubject.next();
      this.apiService.CallApi(this.apiUrl).subscribe((message: string) => {
        console.log(message);
      });
    }
  }

  private appendStatusUpdate(ev: ChannelEvent): void {
    // Just prepend this to the messages string shown in the textarea.
    let date = new Date();
    switch (ev.Data.State) {
      case 'starting': {
        this.messages = `${date.toLocaleDateString()} : complete\n` + this.messages;
        break;
      }
      default: {
        this.messages = `${date.toLocaleTimeString()} : ${ev.Data.State} : ${ev.Data.PercentComplete} % complete\n` + this.messages;
      }
    }
  }
}

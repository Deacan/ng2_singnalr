import { TaskComponent } from './index';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [TaskComponent],
  exports: [TaskComponent]
})
export class TaskModule { }

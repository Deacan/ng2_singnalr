import { EnvConfig } from './env-config.interface';

const BaseConfig: EnvConfig = {
  // Sample API url
  API: 'http://localhost:9123/',
  LongTaskUrl: 'tasks/long',
  ShortTaskUrl: 'tasks/short',
  MediumJobUrl: 'jobs/medium'
};

export = BaseConfig;

